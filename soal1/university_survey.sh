#! /bin/bash

# Top 5 Universities in Japan
echo "Top 5 Universities in Japan"
awk '/JP/' "2023 QS World University Rankings.csv" | sed -n 1,5p | cut -d',' -f2
echo ""
echo ""

# Lowest 5 FSR score Universities in Japan
echo "Lowest 5 FSR score Universities in Japan :"
awk '/JP/' "2023 QS World University Rankings.csv" | sort -t, -k9 -nr | tail -n 5 | sort -t, -k9 -n | cut -d',' -f2
echo ""
echo ""

# Top 10 Universities in Japan sorted by GER ranking
echo "Top 10 Universities in Japan sorted by GER ranking :"
awk '/JP/' "2023 QS World University Rankings.csv" | sed -n 1,10p | sort -t, -k20 -nr | cut -d',' -f2
echo ""
echo ""

# Coolyeah
echo "The Coolyeah :"
awk '/Keren/' "2023 QS World University Rankings.csv" | cut -d',' -f2
echo ""
echo ""