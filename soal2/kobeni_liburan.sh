#!/bin/bash

latestday=$(date +"%d")
firstfolder=1
lastfolder=1

while [ 1 ]
do
  hour=$(date +"%H")
  if [ $hour -eq "00" ]
  then 
    hour=1
  fi
  mkdir "kumpulan_$lastfolder"
  num=1
  while [ $num -le $hour ]
  do
    wget -O "kumpulan_$lastfolder"/"perjalanan_$num.jpg" https://source.unsplash.com/featured/?Indonesia
    num=$((num+1))
  done
  
  currentday=$(date +"%d")
  if [ $currentday -ne $latestday ]
  then
    for ((num=firstfolder; num<=lastfolder; num=num+1))
    do
      zip -r "devil_$num.zip" "kumpulan_$num"
    done
    firstfolder=$((lastfolder+1))
    latestday=$((currentday))
  fi
  lastfolder=$((lastfolder+1))
  sleep 10h
done
