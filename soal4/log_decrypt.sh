#! /bin/bash

# truncate -s 0 /var/log/syslog

# Set the file to the recent log record & get the hour
filePath="$(ls | awk '/.txt/' | tail -n 1)"
getHour=$(ls | awk '/.txt/' | sort -t: -k1 | tail -n 1 | cut -d':' -f1)

# Alphabets that will be decrypted
cypherArtifacts="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

# Decrypt Function, further used in tr command
function decrypt () {
    # Local declaration
    local agentOfDecryption=$1
    local temp=""

    # Separates Upper and Lower cases
    upperBound="$(echo ${cypherArtifacts:26:51})"
    lowerBound="$(echo ${cypherArtifacts:0:26})"

    # Get the range for each case using the substring concept {string:starting:population}
    temp+="${lowerBound:agentOfDecryption%26:26}${lowerBound:0:agentOfDecryption%26}"
    temp+="${upperBound:agentOfDecryption%26:26}${upperBound:0:agentOfDecryption%26}"
    echo $temp
}

# Implement decrypt function on the encrypted file
function ThyFullDecryptionPack () {
    echo -n "$(cat "$filePath" | tr [$(decrypt $getHour)] [$(echo $cypherArtifacts)])"
}

# Call decrypt pack
echo -n "$(ThyFullDecryptionPack)"