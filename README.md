# sisop-praktikum-modul-1-2023-AM-C05

## Soal 1

## Soal 2
Ketika program dieksekusi, program akan mengambil tanggal lalu menyimpannya ke variabel latestday. Variabel firstfolder dan lastfolder digunakan sebagai batas folder mana saja yang akan dizip. Pada perulangan while baris 7, diambil jam untuk menentukan berapa banyak gambar yang perlu diunduh dan disimpan di variabel hour, lalu diunduhlah gambar sebanyak "hour" kali. Selanjutnya, untuk menentukan apakah perlu dilakukan zip, diambil tanggal sekarang lalu disimpan di variabel currentday. Apabila currentday berbeda dari latestday, hal tersebut menandakan bahwa telah terjadi pergantian hari sehingga perlu dilakukan zip. Zip dilakukan dari firstfolder hingga lastfolder. Nilai lastfolder selalu bertambah pada setiap pengulangan while baris 7, sedangkan firstfolder bertambah menjadi lastfolder+1 setelah dilakukan zip. Latestday juga diubah sesuai dengan currentday setelah dilakukan zip. Command sleep pada baris 33 digunakan untuk menunda pengulangan selama 10 jam agar pengunduhan gambar berlangsung setiap 10 jam. Berikut screenshoot setelah program berjalan.
![](images/2-1.png)
![](images/2-2.png)
![](images/2-3.png)
![](images/2-4.png)
## Soal 3

## Soal 4
